document.addEventListener('DOMContentLoaded', () => {
    const themeButton = document.getElementById('theme-button');

    // Check for saved theme in localStorage
    const savedTheme = localStorage.getItem('theme') || 'light';
    document.body.classList.add(savedTheme + '-theme');
    themeButton.classList.add(savedTheme + '-theme');

    themeButton.addEventListener('click', () => {
        if (document.body.classList.contains('light-theme')) {
            switchToDarkTheme();
        } else {
            switchToLightTheme();
        }
    });

    function switchToDarkTheme() {
        document.body.classList.remove('light-theme');
        document.body.classList.add('dark-theme');
        themeButton.classList.remove('light-theme');
        themeButton.classList.add('dark-theme');
        localStorage.setItem('theme', 'dark');
    }

    function switchToLightTheme() {
        document.body.classList.remove('dark-theme');
        document.body.classList.add('light-theme');
        themeButton.classList.remove('dark-theme');
        themeButton.classList.add('light-theme');
        localStorage.setItem('theme', 'light');
    }
});
